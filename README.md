# README #

### Notes ###
* At first the bad things because I will like to take him to light since the beginning. There are some things I could
 have done better and unimplemented functionality. Still I think you can evaluate my code with what has been done.
 
* I really apologize for not implementing Assistance report for many employees, on admin session. 
    I didn't had the time to create the view for selecting employees. However, my plan was to use the 
    ChecksController.checks method, which given an employee and a time range returns the corresponding checks
* I really apologize too for not documenting my API. 
    I use Open-API-Specification for that, with this editor: https://editor.swagger.io/ 
* I'm really sorry but I had issues with the search by gender on the views, so this functionality isn't included.
* This is not the way I commit on my projects. I usually made a commit per each independent change. I did it 
    this way due to lack of time. I don't like to work this unclean way.

* I imagined two or three computers at the entrance of a workplace for the workers to CheckIn/CheckOut, 
    so I tried to create an interface as simple as possible.
* Employees need easy and fast access to CheckIn/CheckOut, so I used a PIN instead of a password 
    for Employee Authentication.
* Internationalization is not implemented.
* I would suggest to create a rails task to check for consistency on CheckIns and Checkouts.
* When deleting a CheckIn or a CheckOut a validation might be included since they are related 
    and data will be inconsistent. I didn't want to make the business too complex so I didn't
    include it. This note is to keep it in mind.
* If I would count with more time, I'd add logs to describe where we are in Backend and how is the data
    state changing.

* I'm a Backend Engineer, tough I can do Frontend if the team really needs it. I focused on Backend on this project
    not in Frontend.

### How do I get set up? ###

* You need docker and docker-compose to run the configured Postgres D.B or configure it by yourself
* To run Postgres Database execute `docker-compose up -d`
* The default admin password is "Hello Runa", after you run `rails db:seed`
require 'rails_helper'

RSpec.describe Check, type: :model do

  it { should validate_presence_of(:type) }
  it { should validate_presence_of(:employees) }

  it { should belong_to(:employees) }
end

require 'rails_helper'

RSpec.describe Employee, type: :model do
  subject { FactoryBot.build(:employees) }

  it { should validate_presence_of(:name) }
  it { should validate_presence_of(:surname) }
  it { should validate_presence_of(:gender) }
  it { should validate_presence_of(:email) }
  it { should validate_presence_of(:pin_digest) }
  it { should validate_presence_of(:code) }

  it { should validate_uniqueness_of(:email).case_insensitive }
  it { should validate_uniqueness_of(:code).case_insensitive }

  it { should have_many(:checks) }
end

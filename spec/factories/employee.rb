FactoryBot.define do
  factory :employees do
    name { Faker::Name.first_name }
    surname { Faker::Name.middle_name + " " + Faker::Name.last_name }
    gender { Faker::Gender }
    email { Faker::Internet.email }
    pin { '1234' }
    pin_confirmation { '1234' }
    code { Faker::String.random(length: 6) }
  end
end
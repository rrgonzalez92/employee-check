require 'rails_helper'

RSpec.describe AdminSessionController, type: :controller do

  describe "GET #dashboard" do
    it "returns http success" do
      get :dashboard
      expect(response).to have_http_status(:success)
    end
  end

  describe "GET #login" do
    it "returns http success" do
      get :login
      expect(response).to have_http_status(:success)
    end
  end

  describe "GET #logout" do
    it "returns http success" do
      get :logout
      expect(response).to have_http_status(:success)
    end
  end

  describe "GET #select_employee_for_report" do
    it "returns http success" do
      get :select_employee_for_report
      expect(response).to have_http_status(:success)
    end
  end

  describe "GET #multiple_employees_report" do
    it "returns http success" do
      get :multiple_employees_report
      expect(response).to have_http_status(:success)
    end
  end

end

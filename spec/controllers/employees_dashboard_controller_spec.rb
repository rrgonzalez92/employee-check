require 'rails_helper'

RSpec.describe EmployeesDashboardController, type: :controller do

  describe "GET #dashboard" do
    it "returns http success" do
      get :dashboard
      expect(response).to have_http_status(:success)
    end
  end

  describe "GET #check_in" do
    it "returns http success" do
      get :check_in
      expect(response).to have_http_status(:success)
    end
  end

  describe "GET #check_out" do
    it "returns http success" do
      get :check_out
      expect(response).to have_http_status(:success)
    end
  end

  describe "GET #assistance_report" do
    it "returns http success" do
      get :assistance_report
      expect(response).to have_http_status(:success)
    end
  end

end

class CreateChecks < ActiveRecord::Migration[6.0]
  def change
    create_table :checks do |t|
      t.references :employee, null: false, foreign_key: true
      t.integer :check_type

      t.timestamps
    end
  end
end

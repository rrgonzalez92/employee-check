class CreateSystemData < ActiveRecord::Migration[6.0]
  def change
    create_table :system_data do |t|
      t.string :password_digest

      t.timestamps
    end
  end
end

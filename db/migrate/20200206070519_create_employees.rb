class CreateEmployees < ActiveRecord::Migration[6.0]
  def change
    create_table :employees do |t|
      t.string :name, null: false
      t.string :surname, null: false
      t.integer :gender, null: false
      t.string :email, null: false, unique: true
      t.string :pin_digest, null: false, default: '1234'
      t.string :code, null: false, index: { unique: true }

      t.timestamps
    end
  end
end

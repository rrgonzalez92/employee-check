Rails.application.routes.draw do
  get 'admin_dashboard/dashboard', as: :admin_dashboard
  get 'admin_dashboard/select_employee_for_report', as: :select_employee_for_report
  get 'admin_dashboard/multiple_employees_report', as: :multiple_employees_report

  get 'employees_dashboard/dashboard', as: 'employees_dashboard'
  #get 'employee_dashboard/check_in'
  #get 'employee_dashboard/check_out'
  #get 'employee_dashboard/assistance_report'
  #get 'admin_session/dashboard'

  #get 'admin_session/select_employee_for_report'
  #get 'admin_session/multiple_employees_report'
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html

  # ERB Controllers
  root 'welcome#index'

  resources :employees

  post 'login', to: 'employees_session#create', as: :employees_login
  get 'logout', to: 'employees_session#destroy', as: :employees_logout

  resources :checks, only: [:create]
  #post 'employees_check_in', to: 'employees_dashboard#check_in', as: 'employees_check_in'
  #post 'employees_check_out', to: 'employees_dashboard#check_out', as: 'employees_check_out'

  get 'employees_report', to: 'employees_dashboard#assistance_report'
  post 'employees_report', to: 'employees_dashboard#assistance_report'

  get 'admin/login', to: 'admin_session#new', as: :admin_login_form
  post 'admin/login', to: 'admin_session#create', as: :admin_login
  get 'admin/logout', to: 'admin_session#destroy', as: :admin_logout

  # API Controllers

end

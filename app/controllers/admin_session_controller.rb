class AdminSessionController < ApplicationController
  def new
  end

  def create
    admin = SystemData.first
    if admin && admin.authenticate(params[:password])
      session[:admin_id] = '2020'
      redirect_to admin_dashboard_path
    else
      flash[:error] = "Contraseña incorrecta."
      redirect_to admin_login_form_path
    end
  end

  def destroy
    session[:admin_id] = nil
    redirect_to root_url
  end
end

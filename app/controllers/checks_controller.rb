class ChecksController < ApplicationController
  before_action :employee_authorized

  def create
    @check = Check.create(employee_id: logged_employee.id)

    last_check = logged_employee.checks&.last

    @checked_out = last_check.nil? || last_check.check_out?

    if @checked_out
      @check.check_in!
      msg = "Entrada registrada a las #{@check.created_at}"
    else
      @check.check_out!
      msg = "Salida registrada a las #{@check.created_at}"
    end

    if @check.valid?
      flash[:notice] = msg
    else
      flash[:errors] = @check.errors.full_messages
    end

    redirect_to employees_dashboard_path
  end

  def self.checks(employee, start_date, end_date)
    employee.checks.where(created_at: start_date..end_date)
  end
end

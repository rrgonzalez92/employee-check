class EmployeesDashboardController < ApplicationController
  before_action :employee_authorized

  def dashboard
    last_check = logged_employee.checks&.last

    @checked_out = last_check.nil? || last_check.check_out?
  end

  def assistance_report
    if params[:date].nil?
      @base_date = Date.today
    else
      @base_date = Date.new.change(month: params[:date][:month].to_i, year: params[:date][:year].to_i)
    end

    start_date = @base_date.beginning_of_month
    end_date = @base_date.end_of_month

    @checks = ChecksController.checks(logged_employee, start_date, end_date)

    separate_in_and_out

    if @check_ins.size - @check_outs.size > 1
      Rails.logger.error "Database is INCONSISTENT. For employee #{logged_employee} the difference " +
                             "between CHECK_INS and CHECK_OUTS is more than 1."
      flash[:error] = 'Hubo un error en la Base de Datos, por favor notifique al personal de tecnología.'
      redirect_to root_path and return
    end

    calculate_hours
  end

  private

  def separate_in_and_out
    @check_ins = []
    @check_outs = []
    @checks.each do |check|
      if check.check_in?
        @check_ins.push(check)
      else
        @check_outs.push(check)
      end
    end
  end

  def calculate_hours
    @total_hours = 0
    @hours = []
    @check_ins.each_with_index do |check_in, i|
      break if @check_outs.size <= i

      diff = TimeDifference.between(@check_outs[i].created_at, check_in.created_at).in_hours
      @total_hours += diff
      @hours.push(diff)
    end
  end
end

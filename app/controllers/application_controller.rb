class ApplicationController < ActionController::Base
  helper_method :logged_employee, :employee_logged_in?

  def logged_employee
    return nil if session[:employee_id].nil?
    @logged_employee ||= Employee.find(session[:employee_id])
  end

  def employee_logged_in?
    logged_employee != nil
  end

  def employee_authorized
    unless employee_logged_in?
      flash[:warning] = 'Por favor, primero identifíquese.'
      redirect_to root_path
    end
  end

  def admin_logged_in?
    !session[:admin_id].nil?
  end

  def admin_authorized
    unless admin_logged_in?
      flash[:warning] = 'Por favor, primero identifíquese.'
      redirect_to admin_login_path
    end
  end
end

class EmployeesController < ApplicationController
  before_action :admin_authorized
  before_action :set_employee, only: [:show, :edit, :update, :destroy]

  def index
    @employees = Employee.all

    @search = params[:search]
    if @search.present?
      @name = @search[:name]
      @surname = @search[:surname]
      @email = @search[:email]

      @gender = @search[:gender]&.to_i

      @employees = Employee.by_name(@name).by_surname(@surname).by_email(@email)
      @employees = @employees.by_gender(@gender) if @gender.present?
    end
  end

  def new
    @employee = Employee.new
  end

  def create
    @employee = Employee.create(employee_params)

    check_result_and_redirect('Empleado registrado', new_employee_path)
  end

  def show
    # set_employee
  end

  def edit
    # set_employee
  end

  def update
    params_to_use = employee_params
    if params[:employee][:pin].blank? && params[:employee][:pin_confirmation].blank?
      params[:employee].delete(:pin)
      params[:employee].delete(:pin_confirmation)
      params_to_use = employee_params_whitout_pin
    end

    @employee.update(params_to_use)

    check_result_and_redirect('Empleado actualizado', edit_employee_path)
  end

  def destroy
    @employee.destroy

    flash[:notice] = 'Empleado eliminado'
    redirect_to employees_path
  end

  private

  def employee_params
    params.require(:employee).permit(
        :name,
        :surname,
        :gender,
        :email,
        :pin,
        :pin_confirmation,
        :code)
  end

  def employee_params_whitout_pin
    params.require(:employee).permit(
        :name,
        :surname,
        :gender,
        :email,
        :code)
  end

  def set_employee
    begin
      @employee = Employee.find(params[:id])
    rescue ActiveRecord::RecordNotFound
      flash[:error] = 'Empleado no encontrado'
      redirect_to employees_path
    end
  end

  def check_result_and_redirect(success_msg, rollback_url)
    if @employee.valid?
      flash[:notice] = success_msg
      redirect_to employees_path
    else
      flash[:errors] = @employee.errors.full_messages
      redirect_to rollback_url
    end
  end

end

class AdminDashboardController < ApplicationController
  before_action :admin_authorized

  def dashboard
  end

  def select_employee_for_report
  end

  def multiple_employees_report
  end
end

class EmployeesSessionController < ApplicationController
  def create
    employee = Employee.find_by_code(params[:code])

    if employee && employee.authenticate_pin(params[:pin])
      session[:employee_id] = employee.id
      redirect_to controller: :employees_dashboard, action: :dashboard
    else
      flash[:error] = "El codigo o el Pin son invalidos"
      redirect_to root_path
    end
  end

  def destroy
    session[:employee_id] = nil
    redirect_to root_url
  end
end

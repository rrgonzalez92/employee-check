class Employee < ApplicationRecord
  enum gender: { :male => 0, :female => 1 }

  has_secure_password :pin

  has_many :checks

  validates_presence_of :name, :surname, :gender, :email, :code
  validates_uniqueness_of :email, case_sensitive: false
  validates_uniqueness_of :code, case_sensitive: false

  validates_presence_of :pin, on: :create
  validates :pin, format: {with: /\A\d+\z/, message: "El PIN puede contener solo dígitos" }, allow_blank: true
  #validates_length_of :pin, minimum: 4, maximum: 4
  validates :pin, length: { is: 4 }, allow_blank: true

  scope :by_name, -> (name) { where("name ILIKE ?", "%#{sanitize_sql_like(name)}%") }
  scope :by_surname, -> (surname) { where("surname ILIKE ?", "%#{sanitize_sql_like(surname)}%") }
  scope :by_email, -> (email) { where("email ILIKE ?", "%#{sanitize_sql_like(email)}%") }
  scope :by_gender, -> (gender) { where(gender: gender) }
end

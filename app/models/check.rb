class Check < ApplicationRecord
  enum check_type: { :check_in => 0, :check_out => 1 }

  belongs_to :employee

  validates_presence_of :check_type, :employee
end
